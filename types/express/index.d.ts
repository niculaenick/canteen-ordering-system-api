// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { User } from 'express';

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  export namespace Express {
    interface User {
      roles?: string[];
    }
  }
}
