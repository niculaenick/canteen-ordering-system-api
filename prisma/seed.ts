import { PrismaClient, User } from '@prisma/client';
import * as faker from 'faker/locale/en';

const prisma = new PrismaClient();

async function main() {
  await createUsers();
  await createRoles();
  await createProducts();
  await createOrdersAndOrderItems();
  await associateUserWithRoles();
}

main()
  .catch((e) => {
    console.log(e);
    process.exit(1);
  })
  .finally(() => {
    prisma.$disconnect();
  });

async function createUsers() {
  const users = Array.from({ length: 100 }).map(() => ({
    email: faker.internet.email(),
    password: faker.internet.password(),
    firstname: faker.name.firstName(),
    lastname: faker.name.lastName(),
  }));

  for (const user of users) {
    await prisma.user.create({
      data: user,
    });
  }
}

async function createRoles() {
  await prisma.role.create({
    data: {
      name: 'ROLE_ADMIN',
    },
  });
  await prisma.role.create({
    data: {
      name: 'ROLE_CUSTOMER',
    },
  });
}
async function associateUserWithRoles() {
  const users: User[] = await prisma.user.findMany();
  for (const user of users) {
    if (user.userId == 1) {
      await prisma.userRole.create({
        data: {
          userId: 1,
          roleId: 1,
        },
      });
    } else {
      await prisma.userRole.create({
        data: {
          userId: user.userId,
          roleId: 2,
        },
      });
    }
  }
}

async function createProducts() {
  const products = Array.from({ length: 100 }).map(() => ({
    name: faker.commerce.productName(),
    description: faker.commerce.productDescription(),
    price: faker.commerce.price(),
    active: true,
  }));

  for (const product of products) {
    await prisma.product.create({ data: product });
  }
}

async function createOrdersAndOrderItems() {
  const orders = Array.from({ length: 30 }).map(() => ({
    userId: faker.datatype.number({ min: 2, max: 100 }),
  }));

  for (const order of orders) {
    const orderInserted = await prisma.order.create({
      data: order,
    });

    const amountItems = faker.datatype.number({ min: 1, max: 5 });
    const orderItems = Array.from({ length: amountItems }).map(() => {
      return {
        quantity: faker.datatype.number({ min: 1, max: 6 }),
        price: '',
        orderId: orderInserted.orderId,
        productId: faker.datatype.number({ min: 1, max: 100 }),
      };
    });

    for (const orderItem of orderItems) {
      const product = await prisma.product.findUnique({
        where: {
          productId: orderItem.productId,
        },
      });

      if (product) {
        orderItem.price = product.price.toString();

        await prisma.orderItem.create({
          data: orderItem,
        });
      }
    }
  }
}
