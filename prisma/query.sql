select
    u.firstname,
    oi.orderItemId,
    oi.productId oi_productId,
    oi.quantity,
    p.productId,
    p.price,
    oi.price,
    o.orderId,
    p.name
from
    User u
    join UserRole ur on ur.userId = u.userId
    join Role r on r.roleId = ur.roleId
    join `Order` o on o.userId = u.userId
    join OrderItem oi on oi.orderId = o.orderId
    join Product p on p.productId = oi.productId
where
    r.roleId = 2
limit
    20;