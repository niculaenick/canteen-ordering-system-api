import { ApiProperty } from '@nestjs/swagger';
import { Prisma } from '@prisma/client';
import { Transform } from 'class-transformer';
import { Allow, IsOptional, IsString, ValidateIf } from 'class-validator';
import { OrderItemEntity } from 'src/order-items/entities/order-item.entity';

export class CreateProductDto {
  @IsString()
  name: string;

  // @ValidateIf((o) => {
  //   return o.description !== null;
  // })
  // @IsString()
  // @IsOptional()
  // @ApiProperty({ required: false })
  description: string | null;
  // @Allow()
  active: boolean;

  // @Transform(({ value }) => value.toNumber())
  // @ApiProperty({ type: Number })
  price: Prisma.Decimal;

  // constructor(partial: Partial<OrderItemEntity>) {
  //   Object.assign(this, partial);
  // }
}
