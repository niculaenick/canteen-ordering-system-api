import { ApiProperty } from '@nestjs/swagger';
import { Prisma, Product } from '@prisma/client';
import { Transform } from 'class-transformer';

export class ProductEntity implements Product {
  productId: number;
  name: string;
  description: string | null;
  active: boolean;
  @Transform(({ value }) => value.toNumber())
  @ApiProperty({ type: Number })
  price: Prisma.Decimal;

  constructor(partial: Partial<ProductEntity>) {
    Object.assign(this, partial);
  }
}
