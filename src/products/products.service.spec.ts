// https://github.com/jmcdo29/testing-nestjs/tree/master/apps/prisma-sample/src/cat
import { Test, TestingModule } from '@nestjs/testing';
import { Prisma, Product } from '@prisma/client';
import { ProductsService } from './products.service';
import { prisma } from '../prisma.service.mock';
import { PrismaService } from '../prisma/prisma.service';

const products = [
  {
    productId: 1,
    name: 'Product 1',
    description: 'Desc prodcut 1',
    active: true,
    price: new Prisma.Decimal(24.45),
  },
  {
    productId: 2,
    name: 'Product 2',
    description: 'Desc product 2',
    active: true,
    price: new Prisma.Decimal(7.32),
  },
];

const product = products[0];
const product2 =
  products[1] as unknown as Prisma.Prisma__ProductClient<Product>;

describe('ProductsService', () => {
  let productService: ProductsService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductsService,
        {
          provide: PrismaService,
          useValue: prisma,
        },
      ],
    }).compile();

    productService = module.get<ProductsService>(ProductsService);
  });

  it('should be defined', () => {
    expect(productService).toBeDefined();
  });

  describe('findOne', () => {
    prisma.product.findUnique.mockResolvedValue(product);
    describe('when product with id exists', () => {
      it('should return a product', async () => {
        expect(await productService.findOne(1)).toEqual(product);
        expect(prisma.product.findUnique).toHaveBeenCalledTimes(1);
        expect(prisma.product.findUnique).toHaveBeenCalledWith({
          where: { productId: 1 },
        });
      });
    });
  });
  describe('findAll', () => {
    prisma.product.findMany.mockResolvedValue(products);
    it('should return an array of products ', async () => {
      expect(await productService.findAll()).toEqual(products);
    });
  });
  describe('create', () => {
    prisma.product.create.mockReturnValue(product2);
    it('should return the created product ', async () => {
      expect(await productService.create(product)).toEqual(product2);
      expect(prisma.product.create).toHaveBeenCalledTimes(1);
    });
  });
  describe('update', () => {
    prisma.product.update.mockResolvedValue(product);
    it('should return the updated product ', async () => {
      expect(await productService.update(1, product)).toEqual(product);
      expect(prisma.product.update).toHaveBeenCalledTimes(1);
    });
  });
  describe('remove', () => {
    prisma.product.delete.mockResolvedValue(product);
    it('should return the removed product ', async () => {
      expect(await productService.remove(1)).toEqual(product);
      expect(prisma.product.delete).toHaveBeenCalledTimes(1);
    });
  });
});
