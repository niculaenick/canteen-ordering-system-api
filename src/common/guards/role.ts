export enum Role {
  Customer = 'ROLE_CUSTOMER',
  Admin = 'ROLE_ADMIN',
}

const roleHierarchy = new Map<Role, Role[]>();

roleHierarchy.set(Role.Customer, [Role.Customer]);
roleHierarchy.set(Role.Admin, [Role.Customer]);

export function isGranted(keyRole: Role, rolesFromDecorator: Role[]): boolean {
  const roles = roleHierarchy.get(keyRole);

  // if (roles) {
  //   return roles.includes(rolesFromDecorator);
  // }
  return false;
}
