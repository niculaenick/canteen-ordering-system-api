import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const rolesFromDecorator = this.getDecoratorRoles(context);

    if (!rolesFromDecorator) {
      return true;
    }
    const userRolesFromJwt = this.getUserRolesFromJwt(context);

    if (!userRolesFromJwt) {
      return true;
    }

    return this.matchRoles(rolesFromDecorator, userRolesFromJwt);
  }

  private getUserRolesFromJwt(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest<Request>();
    const userRoles = request?.user?.roles;
    if (!userRoles || userRoles === []) {
      return false;
    }
    return userRoles;
  }

  matchRoles(rolesFromDecorator, userRolesFromJwt): boolean {
    console.log('match roles triggerd');

    const hasRole = (role) => userRolesFromJwt.includes(role);
    return rolesFromDecorator.some(hasRole);
  }

  getDecoratorRoles(context: ExecutionContext) {
    let roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      roles = this.reflector.get<string[]>('roles', context.getClass());
    }

    if (!roles || roles === []) {
      return false;
    }

    return roles;
  }
}
