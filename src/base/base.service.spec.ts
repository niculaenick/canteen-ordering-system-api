import { BaseService } from './base.service';
import { prisma } from '../prisma.service.mock';
import { Prisma, Product } from '@prisma/client';
import { ProductEntity } from '../products/entities/product.entity';
import { CreateProductDto } from '../products/dto/create-product.dto';
import { UpdateProductDto } from '../products/dto/update-product.dto';

const products = [
  {
    productId: 1,
    name: 'Product 1',
    description: 'Desc prodcut 1',
    active: true,
    price: new Prisma.Decimal(24.45),
  },
  {
    productId: 2,
    name: 'Product 2',
    description: 'Desc product 2',
    active: true,
    price: new Prisma.Decimal(7.32),
  },
];

const product = products[0];
const product2 =
  products[1] as unknown as Prisma.Prisma__ProductClient<Product>;

describe('BaseService', () => {
  let service: BaseService<ProductEntity, CreateProductDto, UpdateProductDto>;

  beforeEach(async () => {
    service = new BaseService(prisma, 'product');
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findOne', () => {
    prisma.product.findUnique.mockResolvedValue(product);
    describe('when product with id exists', () => {
      it('should return a product', async () => {
        expect(await service.findOne(1)).toEqual(product);
        expect(prisma.product.findUnique).toHaveBeenCalledTimes(1);
        expect(prisma.product.findUnique).toHaveBeenCalledWith({
          where: { productId: 1 },
        });
      });
    });
  });
  describe('findAll', () => {
    prisma.product.findMany.mockResolvedValue(products);
    it('should return an array of products ', async () => {
      expect(await service.findAll()).toEqual(products);
    });
  });
  describe('create', () => {
    prisma.product.create.mockReturnValue(product2);
    it('should return the created product ', async () => {
      expect(await service.create(product)).toEqual(product2);
      expect(prisma.product.create).toHaveBeenCalledTimes(1);
    });
  });
  describe('update', () => {
    prisma.product.update.mockResolvedValue(product);
    it('should return the updated product ', async () => {
      expect(await service.update(1, product)).toEqual(product);
      expect(prisma.product.update).toHaveBeenCalledTimes(1);
    });
  });
  describe('remove', () => {
    prisma.product.delete.mockResolvedValue(product);
    it('should return the removed product ', async () => {
      expect(await service.remove(1)).toEqual(product);
      expect(prisma.product.delete).toHaveBeenCalledTimes(1);
    });
  });
});
