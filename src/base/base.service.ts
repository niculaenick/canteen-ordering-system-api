import { PrismaService } from '../prisma/prisma.service';

export class BaseService<E, C, U> {
  id: string;
  constructor(
    protected readonly prisma: PrismaService,
    protected model: string,
  ) {
    this.id = this.model + 'Id';
  }
  async create(createDto: C): Promise<E> {
    const ret = this.prisma[this.model].create({ data: createDto });
    return ret;
  }

  async findAll(): Promise<[E]> {
    return this.prisma[this.model].findMany();
  }

  async findOne(id: number): Promise<E> {
    return this.prisma[this.model].findUnique({ where: { [this.id]: id } });
  }

  async update(id: number, updateDto: U): Promise<E> {
    return this.prisma[this.model].update({
      where: { [this.id]: id },
      data: updateDto,
    });
  }

  async remove(id: number): Promise<E> {
    return this.prisma[this.model].delete({ where: { [this.id]: id } });
  }
}
