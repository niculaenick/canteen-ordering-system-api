import { Injectable } from '@nestjs/common';
import { BaseService } from 'src/base/base.service';
import { PrismaService } from '../prisma/prisma.service';

import { CreateOrderItemDto } from './dto/create-order-item.dto';
import { UpdateOrderItemDto } from './dto/update-order-item.dto';
import { OrderItemEntity } from './entities/order-item.entity';

@Injectable()
export class OrderItemsService extends BaseService<
  OrderItemEntity,
  CreateOrderItemDto,
  UpdateOrderItemDto
> {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma, 'orderItem');
  }
}
