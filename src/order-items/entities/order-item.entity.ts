import { ApiProperty } from '@nestjs/swagger';
import { OrderItem, Prisma } from '@prisma/client';
import { Transform } from 'class-transformer';

export class OrderItemEntity implements OrderItem {
  orderItemId: number;
  quantity: number;
  orderId: number;
  productId: number;
  @Transform(({ value }) => value.toNumber())
  @ApiProperty({ type: Number })
  price: Prisma.Decimal;

  constructor(partial: Partial<OrderItemEntity>) {
    Object.assign(this, partial);
  }
}
