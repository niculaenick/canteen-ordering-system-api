import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrderItemsService } from './order-items.service';
import { CreateOrderItemDto } from './dto/create-order-item.dto';
import { UpdateOrderItemDto } from './dto/update-order-item.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { OrderItemEntity } from './entities/order-item.entity';

@Controller('order-items')
@ApiTags('Order Items')
@ApiBearerAuth('token')
export class OrderItemsController {
  constructor(private readonly orderItemsService: OrderItemsService) {}

  @Post()
  async create(
    @Body() createOrderItemDto: CreateOrderItemDto,
  ): Promise<OrderItemEntity> {
    return this.orderItemsService.create(createOrderItemDto);
  }

  @Get()
  async findAll(): Promise<OrderItemEntity[]> {
    return this.orderItemsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<OrderItemEntity> {
    return this.orderItemsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrderItemDto: UpdateOrderItemDto,
  ): Promise<OrderItemEntity> {
    return this.orderItemsService.update(+id, updateOrderItemDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<OrderItemEntity> {
    return this.orderItemsService.remove(+id);
  }
}
