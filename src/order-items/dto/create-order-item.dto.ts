import { Prisma } from '@prisma/client';

export class CreateOrderItemDto {
  quantity: number;
  price: Prisma.Decimal;
  orderId: number;
  productId: number;
}
