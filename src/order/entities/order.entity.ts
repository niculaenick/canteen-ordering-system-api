import { Order } from '@prisma/client';

export class OrderEntity implements Order {
  handled: boolean;
  orderId: number;
  createdDate: Date;
  userId: number;
}
