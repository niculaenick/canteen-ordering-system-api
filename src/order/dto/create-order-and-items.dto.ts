import { Type } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import { CreateOrderItemDto } from 'src/order-items/dto/create-order-item.dto';
import { CreateOrderDto } from './create-order.dto';

export class CreateOrderAndItemsDto {
  order: CreateOrderDto;

  //   @Type(() => CreateOrderDto)
  //   @ValidateNested()
  //   Order: CreateOrderDto;

  orderItems: CreateOrderItemDto[];
}
