import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { CreateOrderAndItemsDto } from './dto/create-order-and-items.dto';
import { OrderEntity } from './entities/order.entity';
import { OrderItemEntity } from 'src/order-items/entities/order-item.entity';

@Controller('order')
@ApiTags('Order')
@ApiBearerAuth('token')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @Post()
  create(@Body() createOrderDto: CreateOrderDto): Promise<OrderEntity> {
    return this.orderService.create(createOrderDto);
  }
  @Post('order-and-items')
  async createOrderAndItems(
    @Body() createOrderAndItemsDto: CreateOrderAndItemsDto,
  ): Promise<
    OrderEntity & {
      orderItems: OrderItemEntity[];
    }
  > {
    return this.orderService.createOrderAndItems(createOrderAndItemsDto);
  }

  @Get()
  findAll(): Promise<OrderEntity[]> {
    return this.orderService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<OrderEntity> {
    return this.orderService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrderDto: UpdateOrderDto,
  ): Promise<OrderEntity> {
    return this.orderService.update(+id, updateOrderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<OrderEntity> {
    return this.orderService.remove(+id);
  }
}
