import { Injectable } from '@nestjs/common';
import { BaseService } from 'src/base/base.service';
import { PrismaService } from '../prisma/prisma.service';
import { CreateOrderAndItemsDto } from './dto/create-order-and-items.dto';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderEntity } from './entities/order.entity';

@Injectable()
export class OrderService extends BaseService<
  OrderEntity,
  CreateOrderDto,
  UpdateOrderDto
> {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma, 'order');
  }
  async createOrderAndItems(createOrderAndItemsDto: CreateOrderAndItemsDto) {
    //Todo: Extract userId from token
    // createOrderAndItemsDto.userId = 1;

    // retrieve the price from db.Product
    // loop over createOrderAndItemsDto.orderItems en set the price
    createOrderAndItemsDto.order.userId = 1;
    const data = {
      data: {
        ...createOrderAndItemsDto.order,
        orderItems: {
          create: createOrderAndItemsDto.orderItems,
        },
      },
      include: {
        orderItems: true,
      },
    };

    return this.prisma.order.create(data);
  }
}
