import { mockDeep, DeepMockProxy } from 'jest-mock-extended';
import { PrismaService } from './prisma/prisma.service';

export const prisma: DeepMockProxy<PrismaService> = mockDeep<PrismaService>();
