import { Injectable, NotFoundException } from '@nestjs/common';
import { Role } from '@prisma/client';
import { BaseService } from '../base/base.service';
import { PrismaService } from '../prisma/prisma.service';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UsersService extends BaseService<
  UserEntity,
  CreateUserDto,
  UpdateUserDto
> {
  constructor(protected readonly prisma: PrismaService) {
    super(prisma, 'user');
  }

  async findOneByEmailAndPassword(
    email: string,
    password: string,
  ): Promise<UserEntity | null> {
    const user = this.prisma.user.findFirst({
      where: { email: email, password: password },
    });
    if (!user) {
      throw new NotFoundException(
        `User with email ${email} and password ${password} not found.`,
      );
    }
    return user;
  }

  obj = {
    userId: 1,
    email: 'Genesis.Pacocha@gmail.com',
    password: 'faF8AIX6cbBej_w',
    firstname: 'Dayne',
    lastname: 'Beier',
    roles: [
      {
        userId: 1,
        roleId: 1,
        role: {
          roleId: 1,
          name: 'ROLE_ADMIN',
        },
      },
    ],
  };
  // findOneWithRolesByEmailAndPassword
  async findOneWithRolesByEmailAndPassword(email: string, password: string) {
    const user = await this.prisma.user.findFirst({
      include: { roles: { include: { role: true } } },
      where: { email: email, password: password },
    });

    if (!user) {
      return null;
    }

    return this.mapUser(user);
  }

  mapUser(user: any) {
    const { roles, ...userRest } = user;

    const simpleRoles = roles.map((obj) => {
      const {
        role: { name },
      } = obj;
      return name;
    });

    userRest['roles'] = simpleRoles;

    return userRest;
  }

  async createUserAndAssociateWithRole(
    role: string,
    createUserDto: CreateUserDto,
  ) {
    const roleObject = await this.prisma.role.findFirst({
      where: { name: role },
    });

    if (!roleObject) {
      throw new Error('Role not found');
    }
    return await this.prisma.user.create({
      data: {
        ...createUserDto,
        roles: {
          create: [{ roleId: roleObject.roleId }],
        },
      },
      include: {
        roles: true, // Include all roles in the returned object
      },
    });
  }
}
