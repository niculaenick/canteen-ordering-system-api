import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Public } from '../common/decorators/public.decorator';
import { Role } from '../common/guards/role';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
import { UsersService } from './users.service';

@Controller('users')
@ApiTags('Users')
export class UsersController {
  constructor(private readonly service: UsersService) {}

  @ApiBearerAuth('token')
  @Post()
  async create(@Body() createDto: CreateUserDto): Promise<UserEntity> {
    return this.service.create(createDto);
  }

  @Public()
  @Post('/roles')
  async createUserAndAssociateWithRole(
    @Body() createDto: CreateUserDto,
  ): Promise<UserEntity> {
    return this.service.createUserAndAssociateWithRole(
      Role.Customer,
      createDto,
    );
  }
  @Get('/roles')
  findOneWithRolesByEmailAndPassword(@Body() loginDto: LoginUserDto) {
    // Genesis.Pacocha@gmail.com
    // faF8AIX6cbBej_w
    return this.service.findOneWithRolesByEmailAndPassword(
      loginDto.email,
      loginDto.password,
    );
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<UserEntity> {
    return this.service.findOne(+id);
  }

  @Get()
  async findAll(): Promise<UserEntity[]> {
    return this.service.findAll();
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDto: UpdateUserDto,
  ): Promise<UserEntity> {
    return this.service.update(+id, updateDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<UserEntity> {
    return this.service.remove(+id);
  }
}
