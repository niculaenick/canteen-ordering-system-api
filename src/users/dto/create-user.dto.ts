export class CreateUserDto {
  email: string;
  password: string;
  firstname: string | null;
  lastname: string | null;
}
