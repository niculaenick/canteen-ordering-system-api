import { User } from '@prisma/client';

export class UserEntity implements User {
  userId: number;
  email: string;
  password: string;
  firstname: string | null;
  lastname: string | null;
}
