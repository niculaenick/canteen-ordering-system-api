import { Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { Public } from 'src/common/decorators/public.decorator';
import { Roles } from 'src/common/decorators/roles.decorator';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from '../common/guards/local-auth.guard';
import { ApiBearerAuth, ApiBody, ApiParam, ApiTags } from '@nestjs/swagger';
import { Role } from '../common/guards/role';
import { CreateOrderDto } from '../order/dto/create-order.dto';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { LoginUserDto } from '../users/dto/login-user.dto';

@Controller('auth')
@ApiTags('Authorization')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @Post('/register')
  async registerCustomer() {
    return 'Register frate';
  }

  // curl -X POST http://localhost:3000/auth/login -d '{"email": "Genesis.Pacocha@gmail.com", "password": "faF8AIX6cbBej_w"}' -H "Content-Type: application/json"

  // Genesis.Pacocha@gmail.com
  // faF8AIX6cbBej_w

  @Public()
  @UseGuards(LocalAuthGuard)
  @Post('/login')
  @ApiBody({
    type: LoginUserDto,
    description: 'Login',
  })
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @Roles(Role.Customer, Role.Admin)
  @ApiBearerAuth('token')
  @Get('/admin')
  onlyAdmin(@Request() req) {
    return req.user;
  }
}
